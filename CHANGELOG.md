# Changelog

All notable changes to this project will be documented in this file. See [standard-version](https://github.com/conventional-changelog/standard-version) for commit guidelines.

### [0.8.8](https://gitlab.com/9Lukas5/jaad/-/compare/0.8.7...0.8.8) (2022-01-14)


### Features

* **pom:** remove maven gpg and nexus staging plugins from dependencies ([83b48e7](https://gitlab.com/9Lukas5/jaad/-/commit/83b48e74c77d2d46a3c0d6fad0d741a2d3da506f))

### [0.8.7](https://gitlab.com/9Lukas5/jaad/-/compare/0.8.6...0.8.7) (2022-01-14)


### Features

* **ci:** import maven deploy build pipeline ([8571ded](https://gitlab.com/9Lukas5/jaad/-/commit/8571deda983d05a5cddeb60fb0d8f2d6a4f3edde))


### Bug Fixes

* (some) m4a files not playable ([f02aadc](https://gitlab.com/9Lukas5/jaad/-/commit/f02aadcae2ea7ddf1c904a8a9313b2f02d8a3e5b))
