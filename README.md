# JAADec

**This is a fork of https://github.com/DV8FromTheWorld/JAADec, which is a fork of https://sourceforge.net/projects/jaadec/ originally**

Target for my additional fork was to bring it to Maven Central in a working version,
as I had problems with the currently available versions on Maven Central from others I could find
and rather would have my other project depend on a Central dependency than adding an external repository.

## About library

JAAD is an AAC decoder and MP4 demultiplexer library written completely in Java. It uses no native libraries, is platform-independent and portable. It can read MP4 container from almost every input-stream (files, network sockets etc.) and decode AAC-LC (Low Complexity) and HE-AAC (High Efficiency/AAC+).

## Publishes from this repo

Publishes from this repo will be available in the Maven Central repository under

```xml
<dependency>
    <groupId>com.gitlab.9lukas5</groupId>
    <artifactId>jaad</artifactId>
    <!-- check latest version on https://mvnrepository.com or try the latest tag you find in git -->
</dependency>
```
